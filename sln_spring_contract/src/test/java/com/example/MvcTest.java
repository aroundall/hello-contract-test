package com.example;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import org.amuji.todo.TodoService;
import org.junit.Before;

/**
 * Roy Yang
 * 11/08/2017
 */
public class MvcTest {
    @Before
    public void setup() {
        RestAssuredMockMvc.standaloneSetup(new TodoService());
    }
}
