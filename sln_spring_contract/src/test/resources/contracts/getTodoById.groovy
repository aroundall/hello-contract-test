org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url '/todos/1'
        headers {
//            header('Content-Type', 'application/vnd.fraud.v1+json')
        }
    }
    response {
        status 200
        body("""
            {"id":1,"name":"TODO-1"}
        """)
        headers {
            header('Content-Type': 'application/json;charset=UTF-8')
        }
    }
}