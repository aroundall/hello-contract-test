package org.amuji.todo;

/**
 * Roy Yang
 * 11/08/2017
 */
public class Todo {
    private Long id;
    private String name;

    public Todo() {
    }

    public Todo(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Todo setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Todo setName(String name) {
        this.name = name;
        return this;
    }
}
