package org.amuji.todo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Roy Yang
 * 11/08/2017
 */
@RestController
@RequestMapping("/todos")
public class TodoService {

    @GetMapping("{id}")
    public ResponseEntity<Todo> get(@PathVariable String id) {
        Todo todo = new Todo("TODO-" + id).setId(Long.valueOf(id));
        // find the to-do from database, and return to client.
        return ResponseEntity.ok(todo);
    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestBody Todo todo) throws URISyntaxException {
        // save to database,
        // get the persisted ID;
        return ResponseEntity.created(new URI("/todos/1")).build();
    }
}
