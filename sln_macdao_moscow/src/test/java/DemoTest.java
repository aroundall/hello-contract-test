import com.github.macdao.moscow.ContractAssertion;
import com.github.macdao.moscow.ContractContainer;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertNotNull;

/**
 * Roy Yang
 * 16/08/2017
 */
public class DemoTest {
    private static final ContractContainer contractContainer = new ContractContainer(Paths.get("src/test/resources/contracts"));

    @Test
    public void should_response_the_todo_details() throws Exception {
        new ContractAssertion(contractContainer.findContracts("should_response_the_todo_details"))
                .assertContract();
    }

    @Test
    public void should_create_the_todo_successful() throws Exception {
        String todoId = new ContractAssertion(contractContainer.findContracts("should_create_the_todo_successful"))
                .assertContract().get("todo-id");
        assertNotNull(todoId);
    }

}
